#!/usr/bin/env python3
import sys
import traceback
from hyprpy import Hyprland


class App:
    def __init__(self):
        self.pinned_workspaces = {
            "1": lambda monitors: monitors.center(),
            "2": lambda monitors: monitors.center(),
            "3": lambda monitors: monitors.center(),
            "4": lambda monitors: monitors.center(),
            "5": lambda monitors: monitors.center(),
            "6": lambda monitors: monitors.center(),
            "coms": lambda monitors: monitors.right(),
            "www": lambda monitors: monitors.right(),
            "misc": lambda monitors: monitors.right(),
            "7": lambda monitors: monitors.left(),
            "8": lambda monitors: monitors.left(),
            "9": lambda monitors: monitors.left(),
        }
        print(Hyprland)
        self.instance = Hyprland()
        super().__init__()

    def run(self):
        self.on_connect()
        self.instance.signal_workspace_created.connect(self.on_createworkspace)
        self.instance.signal_monitor_added.connect(self.on_monitoradded)
        self.instance.signal_monitor_removed.connect(self.on_monitorremoved)
        self.instance.watch()
        return 0

    def on_connect(self):
        self.refresh_monitors()
        self.rearrange_workspaces()

    def on_monitoradded(self, sender, **kwargs):
        self.refresh_monitors()
        self.rearrange_workspaces()

    def on_monitorremoved(self, sender, **kwargs):
        self.refresh_monitors()
        self.rearrange_workspaces()

    def on_createworkspace(self, sender, **kwargs):
        try:
            workspace = next(
                filter(lambda workspace: workspace.id == kwargs.get('created_workspace_id'), self.instance.get_workspaces()))
        except:
            traceback.print_exc()
            return
        self.move_to_expected_monitor(workspace)

    def monitors(self):
        if not self._monitors:
            self.refresh_monitors()
        return self._monitors

    def refresh_monitors(self):
        self._monitors = Monitors(self.instance.get_monitors())
        print("mainline monitors", self._monitors.mainlineMonitors)
        print("left monitor", self._monitors.left())
        print("center monitor", self._monitors.center())
        print("right monitor", self._monitors.right())

    def rearrange_workspaces(self):
        workspaces = self.instance.get_workspaces()
        for workspace in workspaces:
            self.move_to_expected_monitor(workspace)

    def move_to_expected_monitor(self, workspace):
        if workspace.name not in self.pinned_workspaces:
            return

        expected_monitor = self.pinned_workspaces[workspace.name](self._monitors)
        if workspace.monitor_name == expected_monitor.name:
            return

        self.instance.dispatch([
            'moveworkspacetomonitor', workspace.name, expected_monitor.name
        ])


class Monitors:
    def __init__(self, monitors):
        self.mainlineMonitors = sorted(filter(lambda monitor: abs(monitor.position_y) < 1000, monitors),
                                       key=lambda monitor: monitor.position_x)
        self.secondaryMonitors = filter(lambda monitor: abs(monitor.position_y) >= 100, monitors)
        pass

    def left(self):
        return self.mainlineMonitors[0]

    def center(self):
        if len(self.mainlineMonitors) <= 1:
            return self.mainlineMonitors[0]

        return self.mainlineMonitors[1]

    def right(self):
        if len(self.mainlineMonitors) <= 1:
            return self.mainlineMonitors[0]

        if len(self.mainlineMonitors) <= 2:
            return self.mainlineMonitors[1]

        return self.mainlineMonitors[2]


if __name__ == '__main__':
    app = App()
    sys.exit(
        app.run()
    )
